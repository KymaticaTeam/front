import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {
  DashboardComponent,
  SidebarComponent,
  TopNavComponent,
  FooterComponent,
  InvoicesComponent,
  InvoiceComponent,
  InvoiceFormComponent,
  InvoiceService,
  CorrectiveNotesComponent,
  CorrectiveNoteFormComponent
} from './dashboard';
import {
  FiltersComponent,
  UrlParameterBinService
} from './helpers';
import { PreloaderComponent } from './preloader/preloader.component';
import { ExportComponent } from './dashboard/export/export.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SidebarComponent,
    TopNavComponent,
    FooterComponent,
    InvoicesComponent,
    InvoiceComponent,
    InvoiceFormComponent,
    FiltersComponent,
    PreloaderComponent,
    CorrectiveNotesComponent,
    CorrectiveNoteFormComponent,
    ExportComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [InvoiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
