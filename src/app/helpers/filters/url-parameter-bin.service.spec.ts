import { TestBed } from '@angular/core/testing';

import { UrlParameterBinService } from './url-parameter-bin.service';

describe('UrlParameterBinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlParameterBinService = TestBed.get(UrlParameterBinService);
    expect(service).toBeTruthy();
  });
});
