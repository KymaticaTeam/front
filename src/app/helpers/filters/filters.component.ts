import { Component, ViewChild, OnInit, Input, ElementRef, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { UrlParameterBinService } from './url-parameter-bin.service';
import { HeadersService } from '../../../_services';

declare var $: any;
@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})

export class FiltersComponent implements OnInit {
  readonly apiUrl: string = environment.apiUrl;
  filters: any;
  querryUrl: any = [];
  qr: {};
  qstring: string = '?';
  myModel: any;
  route: string;
  currentURL = '';
  componenthref = '';
  params: any = [];
  pages: number;
  filterbuffer: {};
  invoiceOwners: any = [];
  paginationData: any = {
    "page": {
      "order": 0,
      "type": "text",
      "class": "form-control hidden",
      "placeholder": "Page",
      "label": "Page"
    },
    "itemsPerPage": {
      "order": 0,
      "type": "text",
      "class": "form-control hidden",
      "placeholder": "Items per page",
      "label": "Items per page"
    }
  };

  @Input() pagination: object = {};
  @Input() showPagination: boolean;

  constructor(private http: HttpClient,
    public headersService: HeadersService,
    public filtersBin: UrlParameterBinService,
    private activatedRoute: ActivatedRoute) {
    this.currentURL = window.location.href;
    this.filterbuffer = {};
    this.activatedRoute.queryParams.subscribe(params => {
      this.params = params;
    });
  }

  ngOnInit() {
    this.pages = parseInt(localStorage.getItem("pages"));
    this.http.get(this.apiUrl + '/owners', { headers: this.headersService.onlyContent() })
    .subscribe(data => {
      this.invoiceOwners = data["ownersList"];
    }, error => {
      console.log(error);
    });
  }
  
  keyPress(event: KeyboardEvent) {
    if(event.keyCode == 13){
      this.search();
    }
}

  @Output() valueChange = new EventEmitter();

  @ViewChild('someInput', { static: false }) someInput: ElementRef;

  onChange(key, value) {
    value = value.target.value;
    if (value != null && value != '' && value != '0' && typeof value != 'undefined') {
      this.filtersBin.addParam(key, value);
    } else {
      this.filtersBin.removeParam(key);
    }
    this.filtersBin.removeParam("page");
    //this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
  }

  setPage(page, alg) {
    if(alg == 'inc'){
      page = parseInt(page) + 1;
    }else{
      page = parseInt(page) - 1;
    }
      this.filtersBin.addParam("page", page);
      this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
  }
  setItemsPerPage(items) {
    this.filtersBin.addParam("itemsPerPage", items);
    this.filtersBin.removeParam("page");
    this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
  }
  
  setSort(sort) {
    this.filtersBin.addParam("sort", sort);
    this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
  }

  makeQuerry(name) {
    let qarr = [];
    this.qr = { ...this.qr, ...name };
    for (let key in this.qr) {
      let value = this.qr[key];
      if (value && value != 0) {
        qarr.push(key + '=' + value);
      }
    }
    return this.qstring = '?' + qarr.join('&');
  }

  descOrder = (a, b) => {
    if (a.value.order > b.value.order) return a.value;
  }
  
  clearSearch(){
    this.filtersBin.clearParams();
    this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
    window.location.reload();
  }
  
  search(){
    this.valueChange.emit(this.filtersBin.getQueryStringAndSetUrl());
  }
}