import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class UrlParameterBinService {
    paramsObject: any;
    queryString: any;
    operationTitle: any;
    initialDocumentRoot: any;

    constructor(
        public route: ActivatedRoute,
        public router: Router
    ) {
        //console.log(this.router.events.subscribe)
        this.router.events.subscribe((event) => {

            if (typeof event['urlAfterRedirects'] != 'undefined' && event['urlAfterRedirects'] != '') {
                this.paramsObject = UrlParameterBinService.getAllUrlParams(event['urlAfterRedirects']);
                this.queryString = UrlParameterBinService.makeQueryString(this.paramsObject);
                this.operationTitle = "Undefined title";
                this.initialDocumentRoot = window.location.pathname;
            }

        });

    }

    public static getAllUrlParams(currentRoute) {
        let url = currentRoute;
        let queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        let obj = {};

        // if query string exists
        if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            let arr = queryString.split('&');

            for (let i = 0; i < arr.length; i++) {
                // separate the keys and the values
                let a = arr[i].split('=');

                // set parameter name and value (use 'true' if empty)
                let paramName = a[0];
                let paramValue = typeof (a[1]) === 'undefined' ? true : decodeURIComponent(a[1]);

                // if the paramName ends with square brackets, e.g. colors[] or colors[2]
                if (paramName.match(/\[(\d+)?\]$/)) {

                    // create key if it doesn't exist
                    let key = paramName.replace(/\[(\d+)?\]/, '');
                    if (!obj[key]) obj[key] = [];

                    // if it's an indexed array e.g. colors[2]
                    if (paramName.match(/\[\d+\]$/)) {
                        // get the index value and add the entry at the appropriate position
                        let index = /\[(\d+)\]/.exec(paramName)[1];
                        obj[key][index] = paramValue;
                    } else {
                        // otherwise add the value to the end of the array
                        obj[key].push(paramValue);
                    }
                } else {
                    // we're dealing with a string
                    if (!obj[paramName]) {
                        // if it doesn't exist, create property
                        obj[paramName] = paramValue;
                    } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                        // if property does exist and it's a string, convert it to an array
                        obj[paramName] = [obj[paramName]];
                        obj[paramName].push(paramValue);
                    } else {
                        // otherwise add the property
                        obj[paramName].push(paramValue);
                    }
                }
            }
        }

        return obj;
    }

    public setParams(params) {
        this.paramsObject = params;
    }

    public addParam(name, value) {
        this.paramsObject[name] = value;
    }

    public clearParams() {
        this.paramsObject = {};
    }

    public appendParams(params) {
        if (typeof params === 'undefined' || Object.keys(params).length === 0) {
            this.clearParams();
            this.setUrlState();
            return;
        }

        Object.assign(this.paramsObject, params);
        this.setUrlState();
    }


    public removeParam(name) {
        delete this.paramsObject[name];
    }

    public getParams() {
        return this.paramsObject;
    }

    public setUrlState() {
        let queryString = UrlParameterBinService.makeQueryString(this.paramsObject);

        if (queryString === '') {
            history.pushState(this.paramsObject, this.operationTitle, this.initialDocumentRoot);
        }
        else {
            history.pushState(this.paramsObject, this.operationTitle, this.initialDocumentRoot + queryString);
        }
    }

    public getQueryString() {
        return this.queryString;
    }

    public getQueryStringAndSetUrl() {
        this.queryString = UrlParameterBinService.makeQueryString(this.paramsObject);
        this.setUrlState();

        return this.queryString;
    }

    public static makeQueryString(paramsObject) {
        let esc = encodeURIComponent;
        let queryString = Object.keys(paramsObject).map(k => esc(k) + '=' + esc(paramsObject[k])).join('&');

        if (queryString.length > 0) {
            return '?' + queryString;
        }
        else {
            return '';
        }
    }

    public static makeDecodedQueryString(paramsObject) {

        let queryStringArray = [];

        Object.entries(paramsObject).forEach((v) => {
            queryStringArray.push(decodeURIComponent(v[0]) + '=' + v[1]);
        });

        if(queryStringArray.length > 0){
            return '?' + queryStringArray.join('&');
        }else{
            return '';
        }

    }
}
