import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent, InvoicesComponent, InvoiceComponent, CorrectiveNotesComponent } from './dashboard';
import { ExportComponent } from './dashboard/export/export.component';



const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'invoices', component: InvoicesComponent },
  { path: 'corrective_notes', component: CorrectiveNotesComponent },
  { path: 'invoices/details/:id', component: InvoiceComponent },
  { path: 'export', component: ExportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
