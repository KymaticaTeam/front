export const inputMasks = {
    orderNumber: [/[0-9],*/],
    text: [/^[a-zA-Z]/],
    phone: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
};