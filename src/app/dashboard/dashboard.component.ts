import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  location: Location;
  constructor(
    private route: ActivatedRoute,
    location: Location
  ) {
    // localStorage.setItem('userId', null);
    // if (localStorage.getItem("userId") === null) {
    //   alert("d");
    //   //this.location.go('http://gmapl.gmoto.pl/?identify=1');
    // }else{
      
    // }
    this.route.queryParams.subscribe(params => {
      if (params['userId']) {
        localStorage.setItem('userId', params['userId']);
      } else {
        if (localStorage.getItem("userId") === null) {
          if (environment.production) window.location.href = 'http://gmapl.gmoto.pl?identify=1';
          else localStorage.setItem('userId', '1');
        }
      }
    });
  }
  ngOnInit(): void {
    console.log(localStorage.getItem("userId"));
  }

}
