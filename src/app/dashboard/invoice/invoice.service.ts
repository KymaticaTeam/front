import { Injectable, Output, EventEmitter } from '@angular/core'

@Injectable()
export class InvoiceService {

    origin: any = null;

    @Output() change: EventEmitter<boolean> = new EventEmitter();

    initOrigin(origin) {
        this.origin = origin;
        this.change.emit(this.origin);
    }

    refreshList() {
        this.origin = null;
        this.change.emit(this.origin);
    }

}