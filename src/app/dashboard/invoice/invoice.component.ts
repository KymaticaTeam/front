import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { InvoicesComponent } from "./../invoices/invoices.component";

declare var $: any;

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  correct: boolean = false;
  apiUrl = environment.apiUrl;
  invoiceNumber: string = null;
  orderNumber: string = null;
  createdDate: string = null;
  billDate: string = null;
  dueDate: string = null;
  paymentMethod: string = null;
  ownerName: string = null;
  ownerAddress: string = null;
  ownerTaxNumber: string = null;
  ownerPhoneNumber: string = null;
  ownerBdo: string = null;
  payerTaxNumber: string = null;
  payerCompany: string = null;
  payerName: string = null;
  payerAddressStreet: string = null;
  payerAddressBuild: string = null;
  payerAddressFlat: string = null;
  payerAddressPostCode: string = null;
  payerAddressCity: string = null;
  payerAddressCountry: string = null;
  payerPhoneNumber: string = null;
  payerEmail: string = null;
  customerData: boolean = false;
  customerTaxNumber: string = null;
  customerName: string = null;
  customerAddressStreet: string = null;
  customerAddressBuild: string = null;
  customerAddressFlat: string = null;
  customerAddressPostCode: string = null;
  customerAddressCity: string = null;
  customerAddressCountry: string = null;
  customerPhoneNumber: string = null;
  total: number = null;
  totalWord: string = null;
  totalCorrectWord: string = null;
  items: any = null;
  oldItems: any = null;
  summaries: any = null;
  invoiceOriginNumber = null;
  invoiceOriginDate = null;
  sumOldNet: number = 0;
  sumOldVat: number = 0;
  sumOldTotal: number = 0;
  invoiceId: number = null;
  currency = 'PLN';

  constructor(
    private http: HttpClient,
    public headersService: HeadersService,
    public InvoicesComponent: InvoicesComponent
  ) { }

  ngOnInit(): void {
  }


  loadInvoice(id: number) {
    this.invoiceId = id;
    this.correct = false;
    this.http.get(this.apiUrl + '/invoice/' + id, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        this.invoiceNumber = data['invoice']['number'];
        this.orderNumber = data['invoice']['orderNumber'];
        this.createdDate = data['invoice']['createdDate'];
        this.billDate = data['invoice']['billDate'];
        this.dueDate = data['invoice']['dueDate'];
        this.paymentMethod = data['invoice']['paymentMethod'];
        this.ownerName = data['invoice']['owner']['name'];
        this.ownerAddress = data['invoice']['owner']['address'];
        this.ownerTaxNumber = data['invoice']['owner']['taxNumber'];
        this.ownerPhoneNumber = data['invoice']['owner']['phoneNumber'];
        this.ownerBdo = data['invoice']['owner']['bdo'];
        this.payerTaxNumber = data['invoice']['payer']['taxNumber'];
        this.payerCompany = data['invoice']['payer']['name'];
        this.payerName = data['invoice']['payer']['company'];
        this.payerAddressStreet = data['invoice']['payer']['address']['street'];
        this.payerAddressBuild = data['invoice']['payer']['address']['build'];
        this.payerAddressFlat = data['invoice']['payer']['address']['flat'];
        this.payerAddressPostCode = data['invoice']['payer']['address']['postCode'];
        this.payerAddressCity = data['invoice']['payer']['address']['city'];
        this.payerAddressCountry = data['invoice']['payer']['address']['country'];
        this.payerPhoneNumber = data['invoice']['payer']['phone'];
        this.payerEmail = data['invoice']['payer']['email'];
        this.totalWord = data['invoice']['totalWord'];
        this.totalCorrectWord = data['invoice']['totalCorrectWord'];
        this.total = data['invoice']['total'];
        this.invoiceOriginNumber = data['invoice']['origin'];
        this.invoiceOriginDate = data['invoice']['originDate'];
        if (data['invoice']['owner']['short'] == 'gmotoGMBH') {
          this.currency = 'EUR';
        }else{
          this.currency = 'PLN';
        }
        if (data['invoice']['origin'] != null) {
          this.correct = true;
        }
        if (data['invoice']['customer'] != null) {
          this.customerData = true;
          this.customerTaxNumber = data['invoice']['customer']['taxNumber'];
          this.customerName = data['invoice']['customer']['name'];
          this.customerAddressStreet = data['invoice']['customer']['address']['street'];
          this.customerAddressBuild = data['invoice']['customer']['address']['build'];
          this.customerAddressFlat = data['invoice']['customer']['address']['flat'];
          this.customerAddressPostCode = data['invoice']['customer']['address']['postCode'];
          this.customerAddressCity = data['invoice']['customer']['address']['city'];
          this.customerAddressCountry = data['invoice']['customer']['address']['country'];
          this.customerPhoneNumber = data['invoice']['customer']['phone'];
        } else {
          this.resetInvoiceData();
        }
        this.items = data['invoice']['items'].sort((a, b) => a.lp - b.lp);
        if (data['invoice']['oldItems'] != null) {
          this.oldItems = data['invoice']['oldItems'].sort((a, b) => a.lp - b.lp);
          data['invoice']['oldItems'].forEach(element => {
            if (element != null) {
              this.sumOldNet = this.sumOldNet + element.net * element.quantity;
              this.sumOldVat = this.sumOldVat + parseFloat(element.sumVat);
              this.sumOldTotal = this.sumOldTotal + parseFloat(element.sumTotal);
            }
          });
        }
        this.summaries = data['invoice']['summary'];
        $('#invoiceModal').modal('show');
        console.log(this.total);
      });
      
  }

  resetInvoiceData() {
    this.customerData = false;
    this.customerName = null;
    this.customerAddressStreet = null;
    this.customerAddressBuild = null;
    this.customerAddressFlat = null;
    this.customerAddressPostCode = null;
    this.customerAddressCity = null;
    this.customerAddressCountry = null;
    this.customerTaxNumber = null;
    this.customerPhoneNumber = null;
    this.sumOldTotal = 0;
    this.sumOldVat = 0;
  }
  sendInvoice(){
    $('#emailInput').modal('show');
  }
  finalSendInvoice(id){
    $('#emailInput').modal('hide');
    $('#invoiceModal').modal('hide');
    this.InvoicesComponent.sendInvoice(id, this.payerEmail);
  }
  setEmail(event){
    this.payerEmail = event.target.value;
  }
}
