import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  actualMonth = '';
  currentDate: any;
  activeRootLink: string;
  constructor() {
    this.activeRootLink = window.location.pathname.split("/")[0] + '/' + window.location.pathname.split("/")[1];
  }

  ngOnInit(): void {
    this.actualMonth = '0' + (new Date().getMonth() + 1).toString().slice(-2);
    this.currentDate = new Date().getFullYear()+'-'+this.actualMonth+'-'+new Date().getUTCDate();
  }

}
