import { Component, ViewChild, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { InvoiceComponent } from "../invoice/invoice.component";
import { InvoiceService } from "../invoice/invoice.service";
import { ActivatedRoute } from '@angular/router';
import { UrlParameterBinService } from '../../helpers/filters/url-parameter-bin.service';
declare var $: any;

@Component({
  selector: 'app-corrective-notes',
  templateUrl: './corrective-notes.component.html',
  styleUrls: ['./corrective-notes.component.css']
})
export class CorrectiveNotesComponent implements OnInit {

  @ViewChild(InvoiceComponent) invoiceComponent;
  invoices: object;
  pagination = null;
  correctiveNotes: object;
  apiUrl = environment.apiUrl;
  link = '';
  queryUrl: string = '';
  preloader: boolean = true;

  constructor(
    private http: HttpClient,
    public headersService: HeadersService,
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    public filtersBin: UrlParameterBinService
  ) { }

  ngOnInit(): void {
    this.preloader = true;
    this.link = UrlParameterBinService.makeQueryString(this.filtersBin.getParams());
    this.http.get(this.apiUrl + '/corrective_notes' + this.link, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        this.correctiveNotes = data['correctiveNotes'];
        this.pagination = data['pagination'];
        this.preloader = false;
      },
        error => {
          this.resetData();
          console.log("EE:", error);
          this.preloader = false;
        });
    this.invoiceService.change.subscribe(origin => {
      this.http.get(this.apiUrl + '/corrective_notes' + this.link, { headers: this.headersService.onlyContent() })
        .subscribe(data => {
          this.correctiveNotes = data['correctiveNotes'];
          this.pagination = data['pagination'];
          this.preloader = false;
        },
          error => {
            this.resetData();
            console.log("EE:", error);
            this.preloader = false;
          });
    });
  }

  showInvoice(id: number) {
    this.invoiceComponent.loadInvoice(id);
  }

  addCorrectiveNote() {
    this.invoiceService.initOrigin(null);
    $('#addInvoice').modal('show');
    this.activateMasks();
  }

  makeCorrectionToInvoice(origin) {
    this.invoiceService.initOrigin(origin);
    $('#addInvoice').modal('show');
  }

  activateMasks() {
    $('input[formcontrolname="orderId"]').mask('000000000000000');
  }

  reloadData(querylink) {
    this.queryUrl = querylink;
    this.ngOnInit();
  }

  resetData() {
    this.invoices = null;
    this.pagination = null;
  }

}
