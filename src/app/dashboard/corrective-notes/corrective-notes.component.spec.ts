import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectiveNotesComponent } from './corrective-notes.component';

describe('CorrectiveNotesComponent', () => {
  let component: CorrectiveNotesComponent;
  let fixture: ComponentFixture<CorrectiveNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CorrectiveNotesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectiveNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
