import { Component, HostBinding, Input } from '@angular/core';
import { FormGroup, FormControl, FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { correctiveNoteModel } from './correctiveNoteModel';

declare var $: any;

@Component({
  selector: 'app-corrective-note-form',
  templateUrl: './corrective-note-form.component.html',
  styleUrls: ['./corrective-note-form.component.css']
})
export class CorrectiveNoteFormComponent {

  currentTab: number = 0;
  invoiceType: number = 1;
  apiUrl = environment.apiUrl;
  invoice: any = null;
  errors: any = [];
  showErrors: boolean = false;
  findOrigin: boolean = false;
  changes: any = {};
  noteData: any = null;

  constructor(
    private http: HttpClient,
    public headersService: HeadersService
  ) {
  }

  correctiveNote = new FormGroup({
    invoiceNumber: new FormControl(''),
    createdDate: new FormControl(''),
    description: new FormControl(''),
    owner: new FormControl(''),
    payer: new FormControl(''),
    customer: new FormControl('')
  });

  ngOnInit() {
  }

  findInvoice(event) {
    var data = JSON.stringify({
      "invoiceNumber": event.target.value
    });

    this.http.post(this.apiUrl + '/invoices/find_by_number', data, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        this.invoice = data["invoice"];
        this.findOrigin = true;
        $(".invoiceNumberInputClass").removeClass("error");
        $(".invoiceNumberInputClass").addClass("success");
        this.checkForm();
      }, error => {
        this.findOrigin = false;
        $(".invoiceNumberInputClass").addClass("error");
        $(".invoiceNumberInputClass").removeClass("success");
        this.checkForm();
      });

  }

  nextPrev(i) {
    if (i == 1) {
      this.checkForm();
      if (this.errors.length > 0) {
        this.showErrors = true;
        return false;
      }
    }
    this.currentTab = this.currentTab + i;
  }

  clearErrors() {
    this.errors = [];
  }

  checkForm() {
    // 2020-09-09
    // console.log(this.correctiveNote.value.createdDate);
    this.clearErrors();
    if (this.findOrigin == false) {
      this.errors.push('Nie znaleziono faktury');
    }
    switch (this.currentTab) {
      case 0:
        if (this.correctiveNote.value.invoiceNumber == null || this.correctiveNote.value.invoiceNumber == '') {
          this.errors.push('Wprowadź numer faktury');
        }
        if (this.correctiveNote.value.createdDate == null || this.correctiveNote.value.createdDate == '') {
          this.errors.push('Wprowadź datę wystawienia noty korygującej');
        }
        if (this.correctiveNote.value.description == null || this.correctiveNote.value.description == '') {
          this.errors.push('Wprowadź powód wystawienie noty korygującej');
        }
        break;
      case 1:

        break;
      case 2:
        break;
      case 3:
        break;
    }
    if (this.errors.length > 0) {
      this.showErrors = true;
    } else {
      this.showErrors = false;
    }
  }

  add() {

    if (this.correctiveNote.value.owner === "" && this.correctiveNote.value.payer === "" && this.correctiveNote.value.customer === "") {
      this.clearErrors();
      this.errors.push('Brak zmian do wygenerowania noty korygującej.');
      this.showErrors = true;
      return false;
    }

    this.changes = [];
    if (this.correctiveNote.value.owner != null || this.correctiveNote.value.owner != '') {
      this.changes.owner = this.correctiveNote.value.owner;
    }
    if (this.correctiveNote.value.payer != null || this.correctiveNote.value.payer != '') {
      this.changes.payer = this.correctiveNote.value.payer;
    }
    if (this.correctiveNote.value.customer != null || this.correctiveNote.value.customer != '') {
      this.changes.customer = this.correctiveNote.value.customer;
    }

    this.noteData = JSON.stringify({
      "createdDate": this.correctiveNote.value.createdDate,
      "invoice": this.invoice.id,
      "reason": this.correctiveNote.value.description,
      "changes": {
        "owner": this.changes.owner,
        "payer": this.changes.payer,
        "customer": this.changes.customer
      }
    });

    this.http.post(this.apiUrl + '/corrective_notes/add', this.noteData, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        console.log(data);
        $('#correctiveNote').modal('hide');
      }, error => {
        console.log(error);
        $('#correctiveNote').modal('hide');
      });

  }

}
