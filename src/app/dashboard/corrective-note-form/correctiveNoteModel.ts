import { formatDate } from '@angular/common';
export const correctiveNoteModel = {
    "payer": {
        "name": null,
        "email": null,
        "phone": null,
        "taxNumber": null,
        "active": 1,
        "isCompany": null,
        "address": {
            "street": null,
            "build": null,
            "flat": null,
            "postCode": null,
            "city": null,
            "country": null
        }
    },
    "id": 0,
    "customer": {
        "name": null,
        "email": null,
        "phone": null,
        "taxNumber": null,
        "active": 1,
        "isCompany": null,
        "address": {
            "street": null,
            "build": null,
            "flat": null,
            "postCode": null,
            "city": null,
            "country": null
        }
    },
    "ownerId": 0,
    "orderNumber": null,
    "createdDate": formatDate(new Date(), 'yyyy-MM-dd', 'en'),
    "billDate": formatDate(new Date(), 'yyyy-MM-dd', 'en'),
    "dueDate": formatDate(new Date(), 'yyyy-MM-dd', 'en'),
    "paidDate": null,
    "status": 1,
    "type": 2,
    "reasonId": null,
    "origin": 0,
    "currency": "1",
    "paymentMethodId": 1,
    "description": "",
    "items": []
};