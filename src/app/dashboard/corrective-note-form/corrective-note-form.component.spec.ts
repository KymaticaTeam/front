import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectiveNoteFormComponent } from './corrective-note-form.component';

describe('CorrectiveNoteFormComponent', () => {
  let component: CorrectiveNoteFormComponent;
  let fixture: ComponentFixture<CorrectiveNoteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrectiveNoteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectiveNoteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
