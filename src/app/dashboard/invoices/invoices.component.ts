import { Component, ViewChild, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { InvoiceComponent } from "../invoice/invoice.component";
import { InvoiceService } from "../invoice/invoice.service";
import { ActivatedRoute } from '@angular/router';
import { UrlParameterBinService } from '../../helpers/filters/url-parameter-bin.service';
import {darkblue} from "color-name";
import {element} from "protractor";
declare var $: any;


@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

  @ViewChild(InvoiceComponent) invoiceComponent;
  invoices: object;
  pagination = null;
  summaryTotal = 0;
  summaryNet = 0;
  summaryVat = 0;
  summaryItems = 0;
  invoice: object;
  apiUrl = environment.apiUrl;
  link = '';
  queryUrl: string = '';
  preloader: boolean = true;
  notification = null;
  visibleSidebar: boolean = false;
  constructor(
    private http: HttpClient,
    public headersService: HeadersService,
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    public filtersBin: UrlParameterBinService
  ) { }

      
  ngOnInit(): void {
    this.preloader = true;
    this.summaryItems = 0;
      this.summaryNet = 0;
      this.summaryVat = 0;
      this.summaryTotal = 0;
    this.link = UrlParameterBinService.makeQueryString(this.filtersBin.getParams());
    this.http.get(this.apiUrl + '/invoices' + this.link, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        this.invoices = data['invoices'];
        this.pagination = data['pagination'];
        this.preloader = false;
        this.summaryTotal = data['summary']['total'];
        this.summaryNet = data['summary']['net'];
        this.summaryVat = data['summary']['vat'];
        data['invoices'].forEach(element => {
          this.summaryItems = this.summaryItems ++;
          // this.summaryTotal = this.summaryTotal + element.total;
          // this.summaryVat = this.summaryVat + element.vat;
          // this.summaryNet = this.summaryNet + element.net;
        });
        for (let i = 1; i < data['invoices'].length ; i++) {
          this.summaryItems = i;
        }
        if(data['invoices'].length == 0){
          this.summaryItems = this.summaryItems;
        }else{
          this.summaryItems = this.summaryItems + 1;
        }
      },
        error => {
          this.resetData();
          console.log("EE:", error);
          this.preloader = false;
        });
    this.invoiceService.change.subscribe(origin => {
      this.summaryItems = 0;
      this.summaryNet = 0;
      this.summaryVat = 0;
      this.summaryTotal = 0;
      this.http.get(this.apiUrl + '/invoices' + this.link, { headers: this.headersService.onlyContent() })
        .subscribe(data => {
          this.invoices = data['invoices'];
          this.pagination = data['pagination'];
          this.preloader = false;
          this.summaryTotal = data['summary']['total'];
          this.summaryNet = data['summary']['net'];
          this.summaryVat = data['summary']['vat'];
          data['invoices'].forEach(element => {
            this.summaryItems = this.summaryItems ++;
            // this.summaryTotal = this.summaryTotal + element.total;
            // this.summaryVat = this.summaryVat + element.vat;
            // this.summaryNet = this.summaryNet + element.net;
          });
          for (let i = 1; i < data['invoices'].length ; i++) {
            this.summaryItems = i;
          }
          if(data['invoices'].length == 0){
            this.summaryItems = this.summaryItems;
          }else{
            this.summaryItems = this.summaryItems + 1;
          }
        },
          error => {
            this.resetData();
            console.log("EE:", error);
            this.preloader = false;
          });
    });
  }

  showInvoice(id: number) {
    this.invoiceComponent.loadInvoice(id);
  }

  addInvoice() {
    this.invoiceService.initOrigin(null);
    $('#addInvoice').modal('show');
    this.activateMasks();
  }

  makeCorrectionToInvoice(origin, mode) {
    if(mode == "edit"){
      origin.mode = 'edit';
    }else{
      origin.mode = 'correction';
    }
    this.invoiceService.initOrigin(origin);
    $('#addInvoice').modal('show');
  }

  activateMasks() {
    $('input[formcontrolname="orderId"]').mask('000000000000000');
  }

  reloadData(querylink) {
    this.queryUrl = querylink;
    this.ngOnInit();
  }

  resetData() {
    this.invoices = null;
    this.pagination = null;
  }
  
  downloadCsv() {
    window.location.href = this.apiUrl + '/invoices/exportToCsv' + this.link;
  }
  downloadOptima() {
    window.location.href = this.apiUrl + '/export' + this.link;
  }
  
  sendInvoice(id, email){
      this.http.get(this.apiUrl + '/invoice/send/' + id + '?email='+ encodeURIComponent(email), { headers: this.headersService.onlyContent() })
      .subscribe(data => {
      $('.notification').addClass('show');
      this.notification = data;
      setTimeout(function(){
        $('.notification').removeClass("show");
      }, 3000);
    },
      error => {
        console.log(error);
      });
    }
  
  hideAlert(){
    $('.notification').removeClass("show");
  }
  
  toggleSidebar(){
    this.visibleSidebar = !this.visibleSidebar;
  }

  testFiltersGet() {

      let link = UrlParameterBinService.makeDecodedQueryString(this.filtersBin.getParams());

      this.http.get(this.apiUrl + '/dev/invoices' + link, { headers: this.headersService.onlyContent() }).subscribe(
          data => {
              console.log('ok, done.', data);
          },
          error => {
              console.log('error response.');
          }
      );

  }
}