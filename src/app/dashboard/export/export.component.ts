import { Component, ViewChild, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { InvoiceComponent } from "../invoice/invoice.component";
import { InvoiceService } from "../invoice/invoice.service";
import { ActivatedRoute } from '@angular/router';
import { UrlParameterBinService } from '../../helpers/filters/url-parameter-bin.service';
declare var $: any;

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  @ViewChild(InvoiceComponent) invoiceComponent;
  invoices: object;
  pagination = null;
  correctiveNotes: object;
  apiUrl = environment.apiUrl;
  link = '';
  queryUrl: string = '';
  preloader: boolean = true;
  
  constructor(
    private http: HttpClient,
    public headersService: HeadersService,
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    public filtersBin: UrlParameterBinService
  ) { }

  ngOnInit(): void {
    this.preloader = false;
  }

}
