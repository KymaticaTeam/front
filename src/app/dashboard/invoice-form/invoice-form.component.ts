import { Component, HostBinding, Input } from '@angular/core';
import {Directive, ElementRef} from '@angular/core';
import { FormGroup, FormControl, FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HeadersService } from '../../../_services';
import { environment } from '../../../environments/environment';
import { invoiceModel } from './invoiceModel';
import { InvoiceService } from "../invoice/invoice.service";
import { InvoicesComponent } from "./../invoices/invoices.component";
declare var $: any;

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent {
  currentDate = null;
  currentTab: number = 0;
  ownerId: number = 0;
  invoiceType: number = 1;
  apiUrl = environment.apiUrl;
  customer: any = null;
  isCompanyPayer: number = 0;
  isCompanyCustomer: number = 0;
  items: any[] = [
    {
      lp: '',
      productName: '',
      productId: null,
      unit: '',
      type: 0,
      quantity: '',
      gtu: '',
      cn: '',
      price: '',
      taxRateId: ''
    }
  ];
  origin: any = null;
  errors: any = [];
  showErrors: boolean = false;
  proform: boolean = false;
  country: string = '--';
  invoiceItems: any = [];
  notification = null;
  doubleCorrections: boolean = false;
  invoiceOwners: any = [];
  
  constructor(
    private http: HttpClient,
    public headersService: HeadersService,
    private invoiceService: InvoiceService,
    public InvoicesComponent: InvoicesComponent
  ) {
    this.currentDate = new Date();
  }

  invoice = new FormGroup({
    orderId: new FormControl(''),
    ownerId: new FormControl(''),
    createdDate: new FormControl(''),
    billDate: new FormControl(''),
    dueDate: new FormControl(''),
    payerName: new FormControl(''),
    payerEmail: new FormControl(''),
    payerPhone: new FormControl(''),
    payerTaxNumber: new FormControl(''),
    payerStreet: new FormControl(''),
    payerBuild: new FormControl(''),
    payerFlat: new FormControl(''),
    payerPostCode: new FormControl(''),
    payerCity: new FormControl(''),
    payerCountry: new FormControl(''),
    description: new FormControl(''),
    customerFirstName: new FormControl(''),
    customerLastName: new FormControl(''),
    customerName: new FormControl(''),
    customerEmail: new FormControl(''),
    customerPhone: new FormControl(''),
    customerTaxNumber: new FormControl(''),
    customerStreet: new FormControl(''),
    customerBuild: new FormControl(''),
    customerFlat: new FormControl(''),
    customerPostCode: new FormControl(''),
    customerCity: new FormControl(''),
    customerCountry: new FormControl(''),
  });

  ngOnInit() {
    this.http.get(this.apiUrl + '/owners', { headers: this.headersService.onlyContent() })
    .subscribe(data => {
      this.invoiceOwners = data["ownersList"];
    }, error => {
      console.log(error);
    });
   
    this.invoiceService.change.subscribe(origin => {
      if ($.isEmptyObject(origin)) {
        this.origin = invoiceModel;
        this.ownerId = 0;
        this.origin.mode = 'new';
        this.items = [
          {
            lp: '',
            productName: '',
            productId: null,
            unit: '',
            type: 0,
            quantity: '',
            gtu: '',
            cn: '',
            price: '',
            taxRateId: ''
          }
        ];
      } else {
        this.origin = origin;
        if ($.isEmptyObject(origin.customer)) {
          this.origin.customer = {
            firstName: null,
            lastName: null,
            name: null,
            email: null,
            phone: null,
            taxNumber: null,
            active: 1,
            isCompany: null,
            address: {
              street: null,
              build: null,
              flat: null,
              postCode: null,
              city: null,
              country: null
            }
          };
        }
        
        this.ownerId = origin.owner.id;
        this.http.get(this.apiUrl + '/items_for_invoice/' + origin.id, { headers: this.headersService.onlyContent() })
          .subscribe(data => {
            this.items = data['items'];
            this.doubleCorrections = data['corrects'];
            if(data['corrects'] === true){
              alert("Ta faktura posiada już jedną korekte, czy mimo wszystko chcesz dodać kolejną ?");
            }
          });
        this.invoiceType = 2;
      }
      this.showErrors = false;
      this.clearErrors();
      this.currentTab = 0;
      $(".nav-link").removeClass("active");
      $(".tab-pane").removeClass("active");
      $(".tab-pane").removeClass("show");
      $("#home-tab").addClass("active");
      $("#main").addClass("active");
      $("#main").addClass("show");
    });
  }

  checkItems() {
    this.items.forEach((item, index) => {
      if (item.productName != '') {
        this.invoiceItems.push(item);
      }
    });
  }


  add() {
    let mess = "";
    if(this.origin.mode == "edit"){
      mess = "Zapisać fakturę?";
    }else{
      mess = "Dodać fakturę?";
    }
    if (confirm(mess) == true) {
    this.checkItems();
    let originId = 0
    if ($.isEmptyObject(this.origin)) {
      originId = 0
    } else {
      originId = this.origin.id;
    }
    
    if(this.origin.mode == "edit"){
      this.http.get(this.apiUrl + '/invoices/remove/' + this.origin.id, { headers: this.headersService.onlyContent() })
      .subscribe(data => {
        this.verifyPayerTaxNumber('payer', this.invoice.value.payerTaxNumber);
        this.verifyPayerTaxNumber('customer', this.invoice.value.customerTaxNumber);
        if (this.invoice.value.customerFirstName) {
          this.customer = {
            "firstName": this.invoice.value.customerFirstName,
            "lastName": this.invoice.value.customerLastName,
            "name": this.invoice.value.customerName,
            "email": this.invoice.value.customerEmail,
            "phone": this.invoice.value.customerPhone,
            "taxNumber": this.invoice.value.customerTaxNumber,
            "active": 1,
            "isCompany": this.isCompanyCustomer,
            "address": {
              "street": this.invoice.value.customerStreet,
              "build": this.invoice.value.customerBuild,
              "flat": this.invoice.value.customerFlat,
              "postCode": this.invoice.value.customerPostCode,
              "city": this.invoice.value.customerCity,
              "country": this.invoice.value.customerCountry
            }
          };
        }
        let number = null;
        if(this.origin.mode == "edit"){
          this.invoiceType = 1;
          this.origin.id = null;
          number = this.origin.number;
        }
        if(this.proform === true){
          this.invoiceType = 3;
        }
        var dataToJson = JSON.stringify({
          "payer": {
            "name": this.invoice.value.payerName,
            "email": this.invoice.value.payerEmail,
            "phone": this.invoice.value.payerPhone,
            "taxNumber": this.invoice.value.payerTaxNumber,
            "active": 1,
            "isCompany": this.isCompanyPayer,
            "street": this.invoice.value.payerStreet,
            "build": this.invoice.value.payerBuild,
            "flat": this.invoice.value.payerFlat,
            "postCode": this.invoice.value.payerPostCode,
            "city": this.invoice.value.payerCity,
            "country": this.country
          },
          "customer": this.customer,
          "ownerId": this.ownerId,
          "orderId": this.invoice.value.orderId,
          "createdDate": this.invoice.value.createdDate,
          "billDate": this.invoice.value.billDate,
          "dueDate": this.invoice.value.dueDate,
          "paidDate": null,
          "status": 1,
          "type": this.invoiceType,
          "reasonId": null,
          "id": null,
          "number": number,
          "origin": this.origin.id,
          "currency": "1",
          "paymentMethodId": 1,
          "description": "",
          "items": this.invoiceItems
        });
        this.http.post(this.apiUrl + '/invoices/add', dataToJson, { headers: this.headersService.onlyContent() })
          .subscribe(data => {
            $('#addInvoice').modal('hide');
            this.items = [
              {
                lp: '',
                productName: '',
                productId: null,
                unit: '',
                type: 0,
                quantity: '',
                gtu: '',
                cn: '',
                price: '',
                taxRateId: ''
              }
            ];
            this.origin = null;
            this.invoiceItems = [];
            this.currentTab = 0;
            this.invoiceService.refreshList();
            $('.notification').addClass('show');
            this.notification = data;
            setTimeout(function(){
              $('.notification').removeClass("show");
            }, 3000);
          }, error => {
            console.log(error);
            this.items = [
              {
                lp: '',
                productName: '',
                productId: null,
                unit: '',
                type: 0,
                quantity: '',
                gtu: '',
                cn: '',
                price: '',
                taxRateId: ''
              }
            ];
          });
      },
      error => {
        console.log(error);
      });
    }else{
      this.verifyPayerTaxNumber('payer', this.invoice.value.payerTaxNumber);
        this.verifyPayerTaxNumber('customer', this.invoice.value.customerTaxNumber);
        if (this.invoice.value.customerFirstName) {
          this.customer = {
            "firstName": this.invoice.value.customerFirstName,
            "lastName": this.invoice.value.customerLastName,
            "name": this.invoice.value.customerName,
            "email": this.invoice.value.customerEmail,
            "phone": this.invoice.value.customerPhone,
            "taxNumber": this.invoice.value.customerTaxNumber,
            "active": 1,
            "isCompany": this.isCompanyCustomer,
            "address": {
              "street": this.invoice.value.customerStreet,
              "build": this.invoice.value.customerBuild,
              "flat": this.invoice.value.customerFlat,
              "postCode": this.invoice.value.customerPostCode,
              "city": this.invoice.value.customerCity,
              "country": this.invoice.value.customerCountry
            }
          };
        }
        let number = null;
        if(this.origin.mode == "edit"){
          this.invoiceType = 1;
          this.origin.id = null;
          number = this.origin.number;
        }
        if(this.proform === true){
          this.invoiceType = 3;
        }
        var dataToJson = JSON.stringify({
          "payer": {
            "name": this.invoice.value.payerName,
            "email": this.invoice.value.payerEmail,
            "phone": this.invoice.value.payerPhone,
            "taxNumber": this.invoice.value.payerTaxNumber,
            "active": 1,
            "isCompany": this.isCompanyPayer,
            "street": this.invoice.value.payerStreet,
            "build": this.invoice.value.payerBuild,
            "flat": this.invoice.value.payerFlat,
            "postCode": this.invoice.value.payerPostCode,
            "city": this.invoice.value.payerCity,
            "country": this.country
          },
          "customer": this.customer,
          "ownerId": this.ownerId,
          "orderId": this.invoice.value.orderId,
          "createdDate": this.invoice.value.createdDate,
          "billDate": this.invoice.value.billDate,
          "dueDate": this.invoice.value.dueDate,
          "paidDate": null,
          "status": 1,
          "type": this.invoiceType,
          "reasonId": null,
          "id": null,
          "number": number,
          "origin": this.origin.id,
          "currency": "1",
          "paymentMethodId": 4,
          "createdBy":localStorage.getItem("userId"),
          "description": "",
          "items": this.invoiceItems,
          "exported": 0
        });
        this.http.post(this.apiUrl + '/invoices/add', dataToJson, { headers: this.headersService.onlyContent() })
          .subscribe(data => {
            $('#addInvoice').modal('hide');
            this.items = [
              {
                lp: '',
                productName: '',
                productId: null,
                unit: '',
                type: 0,
                quantity: '',
                gtu: '',
                cn: '',
                price: '',
                taxRateId: ''
              }
            ];
            this.origin = null;
            this.invoiceItems = [];
            this.currentTab = 0;
            this.invoiceService.refreshList();
            $('.notification').addClass('show');
            this.notification = data;
            setTimeout(function(){
              $('.notification').removeClass("show");
            }, 3000);
          }, error => {
            console.log(error);
            this.items = [
              {
                lp: '',
                productName: '',
                productId: null,
                unit: '',
                type: 0,
                quantity: '',
                gtu: '',
                cn: '',
                price: '',
                taxRateId: ''
              }
            ];
          });
    }
    this.proform = false;
  } else {
    return false;
  }
      
  }

  nextPrev(i) {
    this.currentTab = i;
  }

  verifyPayerTaxNumber(key, value) {
    if (key == 'customer') {
      if (value) {
        this.isCompanyCustomer = 1;
      } else {
        this.isCompanyCustomer = 0;
      }
    } else {
      if (value) {
        this.isCompanyPayer = 1;
      } else {
        this.isCompanyPayer = 0;
      }
    }
  }

  addInvoiceItem() {
    this.items.push({
      lp: '',
      productName: '',
      productId: null,
      unit: '',
      type: 0,
      quantity: '',
      gtu: '',
      cn: '',
      price: '',
      taxRateId: ''
    });
  }

  setInvoiceItem(item, prop, event) {
    this.items[item][prop] = event.target.value;
  }

  setOwner(id): void {
    this.ownerId = id;
    this.checkForm();
  }

  clearErrors() {
    this.errors = [];
  }

  checkForm() {
    this.clearErrors();
    switch (this.currentTab) {
      case 0:
        if (this.ownerId == 0) {
          this.errors.push('Wybierz wystawcę');
        }
        if (this.invoice.value.orderId == null || this.invoice.value.orderId == '') {
          this.errors.push('Wprowadź numer zamówienia');
        }
        if (this.invoice.value.createdDate == null || this.invoice.value.createdDate == '') {
          this.errors.push('Wprowadź datę wystawienia faktury');
        }
        if (this.invoice.value.billDate == null || this.invoice.value.billDate == '') {
          this.errors.push('Wprowadź datę sprzedaży');
        }
        if (this.invoice.value.dueDate == null || this.invoice.value.dueDate == '') {
          this.errors.push('Wprowadź datę przedawnienia faktury');
        }
        break;
      case 1:
        if (this.invoice.value.payerStreet == null) {
          this.errors.push('Wprowadź adres płatnika (Ulica)');
        }
        // if (this.invoice.value.payerBuild == null) {
        //   this.errors.push('Wprowadź adres płatnika (Budynek)');
        // }
        if (this.invoice.value.payerPostCode == null) {
          this.errors.push('Wprowadź adres płatnika (Kod pocztowy)');
        }
        if (this.invoice.value.payerCity == null) {
          this.errors.push('Wprowadź adres płatnika (Miejscowość)');
        }
        break;
      case 2:
        break;
      case 3:
        break;
    }
    if (this.errors.length == 0) {
      this.showErrors = false;
    } else {
      this.showErrors = true;
    }
  }
  setCountry(code) {
    this.country = code;
    this.checkForm();
  }
  totalRefund(i){
    this.items[i]["quantity"] = 0;
    this.items[i]["price"] = 0;
  }
  setValue(key, event) {
    switch (key) {
      case 'orderId': {
        this.origin.orderNumber = event.target.value;
        break;
      }
      case 'createdDate': {
        this.origin.createdDate = event.target.value;
        break;
      }
      case 'billDate': {
        this.origin.billDate = event.target.value;
        break;
      }
      case 'dueDate': {
        this.origin.dueDate = event.target.value;
        break;
      }
      case 'description': {
        this.origin.description = event.target.value;
        break;
      }
      case 'payerFirstName': {
        this.origin.payer.firstName = event.target.value;
        break;
      }
      case 'payerLastName': {
        this.origin.payer.lastName = event.target.value;
        break;
      }
      case 'payerCompany': {
        this.origin.payer.company = event.target.value;
        break;
      }
      case 'payerStreet': {
        this.origin.payer.address.street = event.target.value;
        break;
      }
      case 'payerBuild': {
        this.origin.payer.address.build = event.target.value;
        break;
      }
      case 'payerFlat': {
        this.origin.payer.address.flat = event.target.value;
        break;
      }
      case 'payerPostCode': {
        this.origin.payer.address.postCode = event.target.value;
        break;
      }
      case 'payerCity': {
        this.origin.payer.address.city = event.target.value;
        break;
      }
      case 'payerCountry': {
        this.origin.payer.address.country = event.target.value;
        break;
      }
      case 'payerTaxNumber': {
        this.origin.payer.taxNumber = event.target.value;
        break;
      }
      case 'payerEmail': {
        this.origin.payer.email = event.target.value;
        break;
      }
      case 'payerPhone': {
        this.origin.payer.phone = event.target.value;
        break;
      }
      case 'customerFirstName': {
        this.origin.customer.firstName = event.target.value;
        break;
      }
      case 'customerLastName': {
        this.origin.customer.lastName = event.target.value;
        break;
      }
      case 'customerCompany': {
        this.origin.customer.company = event.target.value;
        break;
      }
      case 'customerStreet': {
        this.origin.customer.address.street = event.target.value;
        break;
      }
      case 'customerBuild': {
        this.origin.customer.address.build = event.target.value;
        break;
      }
      case 'customerFlat': {
        this.origin.customer.address.flat = event.target.value;
        break;
      }
      case 'customerPostCode': {
        this.origin.customer.address.postCode = event.target.value;
        break;
      }
      case 'customerCity': {
        this.origin.customer.address.city = event.target.value;
        break;
      }
      case 'customerCountry': {
        this.origin.customer.address.country = event.target.value;
        break;
      }
      case 'customerTaxNumber': {
        this.origin.customer.taxNumber = event.target.value;
        break;
      }
      case 'customerEmail': {
        this.origin.customer.email = event.target.value;
        break;
      }
      case 'customerPhone': {
        this.origin.customer.phone = event.target.value;
        break;
      }
      default: {
        //statements; 
        break;
      }
    }
  }
  hideAlert(){
    $('.notification').removeClass("show");
  }
  
  removeItem(item){
    this.items.splice(item, 1);
  }
}