import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeadersService {
  appName = environment.appName;
  apiUrl = environment.apiUrl;

  constructor() {
  }

  onlyContent() {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return reqHeader
  }

  onlyAuth() {
    var reqHeader = new HttpHeaders({
      'Authorization': localStorage.getItem('userToken')
    });

    return reqHeader
  }
  jsonAuth() {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json', 'Authorization': localStorage.getItem('userToken')
    });
    return reqHeader
  }
}
